import axios from 'axios';

import * as types from './types';

export const getFeed = token => dispatch => {
  dispatch(request());
  axios
    .get('/feed', {
      headers: {
        Authorization: token,
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    })
    .then(res => {
      dispatch(success(res.data));
    })
    .catch(err => {
      console.error(err);
      dispatch(failure(err));
    });

  function request() {
    return { type: types.FEED_REQUEST };
  }
  function success(res) {
    return { type: types.FEED_SUCCESS, res };
  }
  function failure(error) {
    return { type: types.FEED_FAILURE, error };
  }
};
