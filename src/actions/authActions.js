import * as types from './types';
import { push } from 'connected-react-router';
import authMethods from '../components/AuthMethods';

const Auth = new authMethods();

export const login = (email, password) => dispatch => {
  dispatch(request());
  Auth.login(email, password)
    .then(res => {
      if (Auth.loggedIn()) {
        dispatch(success(res.token, res.userName));
        dispatch(push('/'));
      } else {
        dispatch(failure(res));
      }
    })
    .catch(err => {
      dispatch(failure(err));
    });

  function request() {
    return { type: types.LOGIN_REQUEST };
  }
  function success(token, userName) {
    return { type: types.LOGIN_SUCCESS, token, userName };
  }
  function failure(error) {
    return { type: types.LOGIN_FAILURE, error };
  }
};

export const checkLogin = () => dispatch => {
  Auth.checkAuth().then(res => {
    const token = res.token;
    const userName = res.userName;
    if (token) {
      dispatch({ type: types.LOGIN_SUCCESS, token, userName });
      return true;
    } else {
      return false;
    }
  });
};

export const logout = () => dispatch => {
  Auth.logout().then(() => {
    // history.replace('/signin');
    dispatch({
      type: types.LOGOUT
    });
    dispatch(push('/signin'));
  });
};
