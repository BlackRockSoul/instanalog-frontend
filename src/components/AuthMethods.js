import decode from 'jwt-decode';

export default class AuthMethods {
  constructor(domain) {
    this.domain = domain;
  }

  login = async (emailAddress, password) => {
    const res = await this.fetch('/sign/in', {
      method: 'POST',
      body: JSON.stringify({
        emailAddress,
        password
      })
    });
    if (!res.token) {
      return;
    } else {
      await this.setToken(res.token);
    }
    return Promise.resolve(res);
  };

  register = async (emailAddress, userName, password) => {
    const res = await this.fetch('/sign/up', {
      method: 'POST',
      body: JSON.stringify({
        emailAddress,
        userName,
        password
      })
    });
    console.log('res: ', res);
    if (await !res.token) {
      return console.error('Token not found');
    } else {
      this.setToken(res.token);
    }
    return Promise.resolve(res);
  };

  loggedIn = () => {
    const token = this.getToken();
    return !!token && !this.isTokenExpired(token);
  };

  isTokenExpired = token => {
    try {
      const decoded = decode(token);
      if (decoded.exp < Date.now() / 1000) {
        return true;
      } else return false;
    } catch (err) {
      console.log('expired check failed! Line 42: AuthService.js');
      return false;
    }
  };

  setToken = idToken => {
    localStorage.setItem('id_token', idToken);
  };

  getToken = () => {
    return localStorage.getItem('id_token');
  };

  checkAuth = () => {
    const token = this.getToken();
    if (token && !this.isTokenExpired(token)) {
      return Promise.resolve({
        token: token,
        userName: decode(token).name
      });
    } else {
      return Promise.resolve(false);
    }
  };

  logout = () => {
    localStorage.removeItem('id_token');
    return Promise.resolve();
  };

  getConfirm = () => {
    // Using jwt-decode npm package to decode the token
    let answer = decode(this.getToken());
    console.log('Recieved answer!');
    return answer;
  };

  fetch = (url, options) => {
    // performs api calls sending the required authentication headers
    const headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    };
    // Setting Authorization header
    // Authorization: Bearer xxxxxxx.xxxxxxxx.xxxxxx
    if (this.loggedIn()) {
      headers['Authorization'] = 'Bearer ' + this.getToken();
    }

    return fetch(url, {
      headers,
      ...options
    })
      .then(this._checkStatus)
      .then(response => response.json());
  };

  _checkStatus = response => {
    // raises an error in case response status is not a success
    if (response.status >= 200 && response.status < 300) {
      // Success status lies between 200 to 300
      return response;
    } else {
      var error = new Error(response.statusText);
      error.response = response;
      throw error;
    }
  };
}
