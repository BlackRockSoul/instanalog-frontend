import * as types from '../actions/types';

const initialState = {
  feedList: {},
  gotFeed: false,
  inProcess: false
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case types.FEED_REQUEST:
      return { ...state, feedList: {}, gotFeed: false, inProcess: true };
    case types.FEED_SUCCESS:
      return {
        feedList: action.res,
        gotFeed: true,
        inProcess: false
      };
    case types.FEED_FAILURE:
      return { ...state, feedList: {}, gotFeed: false, inProcess: false };
    default:
      return state;
  }
}
