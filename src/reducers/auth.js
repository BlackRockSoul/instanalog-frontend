import * as types from '../actions/types';

const initialState = {
  currentUser: {},
  loggedIn: false,
  inProcess: false
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case types.LOGIN_SUCCESS:
      return {
        ...state,
        currentUser: { token: action.token, userName: action.userName },
        loggedIn: true,
        inProcess: false
      };
    case types.LOGOUT:
      return { ...state, currentUser: {}, loggedIn: false, inProcess: false };
    case types.LOGIN_FAILURE:
      return { ...state, currentUser: {}, loggedIn: false, inProcess: false };
    case types.LOGIN_REQUEST:
      return { ...state, currentUser: {}, loggedIn: false, inProcess: true };
    default:
      return state;
  }
}
