import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { Container } from 'react-bootstrap';
import { ConnectedRouter } from 'connected-react-router';

import * as actions from './actions';
import Home from './views/Home';
import Header from './views/Header';
import SignIn from './views/SignIn';
import SignUp from './views/SignUp';

import { history } from './middleware/history';

const App = props => {
  if (!props.loggedIn) {
    props.checkAuth();
  }

  return (
    <ConnectedRouter history={history}>
      <Header />
      <Container className='mt-3 d-flex justify-content-center'>
        <Route
          exact
          path='/'
          render={() =>
            !props.loggedIn ? <Redirect to='/signin' /> : <Home />
          }
        />
        <Route exact path='/signin' component={SignIn} />
        <Route exact path='/signup' component={SignUp} />
      </Container>
    </ConnectedRouter>
  );
};

const mapDispatchToProps = dispatch => ({
  checkAuth: () => dispatch(actions.checkLogin())
});

const mapStateToProps = function(store, ownProps) {
  return {
    loggedIn: store.auth.loggedIn
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
