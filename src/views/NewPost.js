import React, { useEffect, useState } from 'react';
// eslint-disable-next-line no-unused-vars
import { connect } from 'react-redux';
import * as actions from '../actions';
import { Container } from 'react-bootstrap';

const NewPost = props => {
  const [title, setTitle] = useState([]);
  const [post, setPost] = useState([]);

  return (
    <div className='NewPost'>
      <h1>Create new post</h1>
    </div>
  );
};

const mapDispatchToProps = dispatch => ({
  onLogout: () => dispatch(actions.logout())
});

const mapStateToProps = function(store) {
  return {
    auth: store.auth
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewPost);
