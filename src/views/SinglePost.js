import React from 'react';
import Comments from './Comments';
// eslint-disable-next-line no-unused-vars

const SinglePost = props => {
  return (
    <section>
      {props.feed.map((data, key) => (
        <div key={key} className='post-block mb-5'>
          <h2 className='post-title'>{data.title}</h2>
          <a href='/#' className='post-title-user text-right'>
            {data.author.userName}
          </a>
          <img
            src={data.photo}
            alt={'photo' + data.title}
            style={{ width: '100%' }}
          />
          <div className='commentSection'>
            {data.comments && (
              <Comments
                text={{ post: data.post, userName: data.author.userName }}
                comments={data.comments}
              />
            )}
          </div>
        </div>
      ))}
    </section>
  );
};

export default SinglePost;
