import React from 'react';
// eslint-disable-next-line no-unused-vars
import '../css/Home.css';
import Feed from './Feed';
import Menu from './Menu';

const Home = props => {
  return (
    <div className='d-flex col-12 mt-5'>
      <Feed />
      <Menu />
    </div>
  );
};

export default Home;
