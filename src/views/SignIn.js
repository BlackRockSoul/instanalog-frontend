import React, { useState, useLayoutEffect } from 'react';
import { connect } from 'react-redux';
import { Button, Col, Form } from 'react-bootstrap';
import * as actions from '../actions';

const SignIn = props => {
  const [email, setEmail] = useState(0);
  const [pass, setPass] = useState(0);

  useLayoutEffect(() => {
    if (props.loggedIn) {
      props.history.replace('/');
    }
  }, [props.history, props.loggedIn]);

  const handleInput = e => {
    e.preventDefault();
    switch (e.target.id) {
      case 'formEmail':
        setEmail(e.target.value);
        break;

      case 'formPassword':
        setPass(e.target.value);
        break;

      case 'authForm':
        if (!email || !pass) return console.error('Field not matched');
        props.onAuthRequest(email, pass);
        break;

      default:
        break;
    }
  };

  return (
    <div className='d-flex col-12'>
      <Col md={{ span: 4, offset: 4 }}>
        <Form id='authForm' onSubmit={handleInput}>
          <h4 className='text-center'>Sign In</h4>
          <Form.Group controlId='formEmail'>
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type='email'
              placeholder='Enter email'
              onChange={handleInput}
            />
          </Form.Group>

          <Form.Group controlId='formPassword'>
            <Form.Label>Password</Form.Label>
            <Form.Control
              type='password'
              placeholder='Password'
              onChange={handleInput}
            />
          </Form.Group>

          <Button variant='primary' type='submit'>
            Join!
          </Button>
        </Form>
      </Col>
    </div>
  );
};

const mapDispatchToProps = dispatch => ({
  onAuthRequest: (email, password) => dispatch(actions.login(email, password))
});

const mapStateToProps = function(store) {
  return {
    loggedIn: store.auth.loggedIn
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignIn);
