import React, { useEffect } from 'react';
// eslint-disable-next-line no-unused-vars
import { connect } from 'react-redux';
import * as actions from '../actions';
import SinglePost from './SinglePost';

const Feed = props => {
  const { userToken, onGetFeed } = props;
  useEffect(() => {
    onGetFeed(userToken);
  }, [userToken, onGetFeed]);

  return (
    <div className='Home col-7 mr-1 offset-1'>
      {props.feed.gotFeed ? (
        <SinglePost feed={props.feed.feedList} />
      ) : (
        <div>Loading</div>
      )}
    </div>
  );
};

const mapDispatchToProps = dispatch => ({
  onGetFeed: token => dispatch(actions.getFeed(token))
});

const mapStateToProps = store => ({
  userToken: store.auth.currentUser.token,
  feed: store.feed
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Feed);
