import React from 'react';
// eslint-disable-next-line no-unused-vars
import { Form, Button } from 'react-bootstrap';

const CommentsBlock = props => {
  return (
    <section className='commentBlockSection'>
      <Form className='commentForm'>
        <Form.Group controlId='commentForm'>
          <Form.Control
            as='textarea'
            rows='1'
            aria-label='Add a comment…'
            placeholder='Add a comment…'
            autocomplete='off'
            autocorrect='off'
          />
          <Button variant='primary' type='submit'>
            Send
          </Button>
        </Form.Group>
      </Form>
    </section>
  );
};

export default CommentsBlock;
