import React from 'react';
import { connect } from 'react-redux';
import { Navbar, Nav, Container } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';

import * as actions from '../actions';

const Header = props => {
  return (
    <div>
      <Navbar bg='dark' variant='dark' expand='lg'>
        <Container>
          <LinkContainer to='/'>
            <Navbar.Brand href='#home'>InstAnalog</Navbar.Brand>
          </LinkContainer>
          <Navbar.Toggle aria-controls='basic-navbar-nav' />
          <Navbar.Collapse
            id='basic-navbar-nav'
            className='justify-content-end'
          >
            <Nav>
              {!props.auth.loggedIn && (
                <LinkContainer to='/signup'>
                  <Nav.Link href='#'>Sign Up</Nav.Link>
                </LinkContainer>
              )}
              {!props.auth.loggedIn && (
                <LinkContainer to='/signin'>
                  <Nav.Link href='#'>Sign In</Nav.Link>
                </LinkContainer>
              )}
              {props.auth.loggedIn && (
                <Nav.Link href='#' onClick={props.onLogout}>
                  Sign Out
                </Nav.Link>
              )}
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </div>
  );
};

const mapDispatchToProps = dispatch => ({
  onLogout: () => dispatch(actions.logout()),
  checkLogin: () => dispatch(actions.checkLogin())
});

const mapStateToProps = function(store) {
  return {
    auth: store.auth
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Header);
