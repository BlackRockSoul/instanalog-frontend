import React, { useState, useLayoutEffect } from 'react';
import { Button, Col, Form } from 'react-bootstrap';

const SignIn = props => {
  const [email, setEmail] = useState(0);
  const [name, setName] = useState(0);
  const [pass, setPass] = useState(0);
  const [passConf, setPassConf] = useState(0);

  useLayoutEffect(() => {
    if (props.loggedIn) {
      props.history.replace('/');
    }
  }, [props.history, props.loggedIn]);

  const handleInput = e => {
    e.preventDefault();
    switch (e.target.id) {
      case 'formEmail':
        setEmail(e.target.value);
        break;

      case 'formUsername':
        setName(e.target.value);
        break;

      case 'formPassword':
        setPass(e.target.value);
        break;

      case 'formConfirmPassword':
        setPassConf(e.target.value);
        break;

      case 'authForm':
        if (!email || !name || !pass || !passConf)
          return console.error('Field not matched');
        props.onAuthRequest(email, pass);
        break;

      default:
        break;
    }
  };

  return (
    <div className='col-12'>
      <Col md={{ span: 4, offset: 4 }}>
        <Form md={6} id='authForm' onSubmit={handleInput}>
          <h4 className='text-center'>Sign Up</h4>
          <Form.Group controlId='formEmail'>
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type='email'
              placeholder='Enter email'
              onChange={handleInput}
            />
            <Form.Text className='text-muted'>
              We'll never share your email with anyone else.
            </Form.Text>
          </Form.Group>

          <Form.Group controlId='formUsername'>
            <Form.Label>Username</Form.Label>
            <Form.Control
              type='name'
              placeholder='Enter username'
              onChange={handleInput}
            />
          </Form.Group>

          <Form.Group controlId='formPassword'>
            <Form.Label>Password</Form.Label>
            <Form.Control
              type='password'
              placeholder='Password'
              onChange={handleInput}
            />
          </Form.Group>

          <Form.Group controlId='formConfirmPassword'>
            <Form.Label>Confirm password</Form.Label>
            <Form.Control
              type='password'
              placeholder='Confirm password'
              onChange={handleInput}
            />
          </Form.Group>

          <Button variant='primary' type='submit'>
            Submit
          </Button>
        </Form>
      </Col>
    </div>
  );
};

export default SignIn;
