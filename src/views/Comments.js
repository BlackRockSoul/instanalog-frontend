import React from 'react';
import CommentsBlock from './CommentBlock';
// eslint-disable-next-line no-unused-vars

const Comments = props => {
  return (
    <section className='commentSection'>
      <p>
        <a href='/#' className='prof_link'>
          {props.text.userName}
        </a>
        {' ' + props.text.post}
      </p>
      {props.comments.map(
        (comment, index) =>
          comment.author.userName && (
            <p key={index}>{comment.author.userName + ' ' + comment.text}</p>
          )
      )}

      <CommentsBlock />
    </section>
  );
};

export default Comments;
