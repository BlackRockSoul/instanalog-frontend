import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from '../actions';

function withAuth(ComponentToProtect, props) {
  console.log(ComponentToProtect, props);
  return class extends Component {
    constructor() {
      super();
      this.state = {
        redirect: false
      };
    }
    componentWillMount() {
      if (!this.props.loggedIn && !this.props.onCheckLogin()) {
        console.log('NOT AUTHED');
      }
    }
    render() {
      const { redirect } = this.state;
      if (this.props.inProcess) {
        return null;
      }
      if (redirect) {
        return <Redirect to='/login' />;
      }
      return (
        <React.Fragment>
          <ComponentToProtect {...this.props} />
        </React.Fragment>
      );
    }
  };
}

const mapDispatchToProps = dispatch => ({
  onCheckLogin: () => dispatch(actions.checkLogin())
});

const mapStateToProps = function(...args) {
  return {
    auth: args.auth
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withAuth);

// export default withAuth;
