import React from 'react';
import { render } from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunkMiddleware from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import reducer from './reducers';
import { routerMiddleware } from 'connected-react-router';

import 'bootstrap/dist/css/bootstrap.min.css';
import './css/index.css';
import App from './App';
import { history } from './middleware/history';
import * as serviceWorker from './serviceWorker';

const middleware = [thunkMiddleware];

const store = createStore(
  reducer(history),
  composeWithDevTools(applyMiddleware(...middleware, routerMiddleware(history)))
);

let rootElement = document.getElementById('root');
render(
  <Provider store={store}>
    <App />
  </Provider>,
  rootElement
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
